<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Analista;
use App\Models\User;
use App\Models\Vaga;
use Illuminate\Support\Facades\Validator;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;





use JWTAuth;


class VagaController extends Controller
{
    protected $request;
    private $repository;

    public function __construct(Request $request,Vaga $vaga,Analista $analista)
    {
        $this->request = $request;
        $this->repository = $vaga;
        $this->analistaRepo = $analista;



    }


    public function create (){

        $user = $this->request->user();
        $validator =   $this->validateBody();

        if(!$validator->fails()){
            try{

                $data = $this->request->all();


                $vaga = $this->repository->create($data);

                $analista = $this->analistaRepo->where('Id',$user->id)->first();


                DB::update('UPDATE vagas SET analista_id = ?,tipo_vaga_id = ? WHERE Id = ?', [$analista->Id,$data['tipo_vaga_id'],$vaga->id]);

                return response()->json($vaga,200);
            }catch(\Exception $e){
                return response()->json(['msg'=>'Erro ao Registrar  Vaga'],400);
              }
        }else{
            return response()->json($validator->errors(),400);
        }

    }



    public function validateBody(){
       $validate = Validator::make($this->request->all(),[
            'Titulo'=>'required|string',
            'tipo_vaga_id'=>'required|integer',
            'Descricao'=>'required|string',
            'Salario'=>  'required|numeric',
            'Curriculo_Linkedin'=>'required|string',
        ]);

        return $validate;

    }

    public function update($news_id){

        $user = $this->request->user();

        $vaga = $this->repository->where('Id',$news_id)->first();


        if(!$vaga){
            return response()->json([
                'msg'=> 'Não encontrada  Vaga'
            ],404);
        }

        $validator =   $this->validateBody();

        if(!$validator->fails()){
            try{

                $data = $this->request->all();


                $vaga->Titulo = $data['Titulo'];
                $vaga->Descricao = $data['Descricao'];
                $vaga->tipo_vaga_id = $data['tipo_vaga_id'];
                $vaga->Salario = $data['Salario'];
                $vaga->Curriculo_Linkedin = $data['Curriculo_Linkedin'];

                $vaga->save();

                $analista = $this->analistaRepo->where('Id',$user->id)->first();


                DB::update('UPDATE vagas SET analista_id = ?,tipo_vaga_id = ? WHERE Id = ?', [$analista->Id,$data['tipo_vaga_id'],$vaga->id]);

                return response()->json(['msg'=>' Vaga Atualizada com Sucesso!'],200);
            }catch(\Exception $e){

                return response()->json(['msg'=>'Erro ao Atualizar Vaga'],400);
              }
        }else{
            return response()->json($validator->errors(),400);
        }


    }

    public function delete($news_id){

        $user = $this->request->user();

        $vaga = $this->repository->where('Id',$news_id)->first();

        if(!$vaga){
            return response()->json([
                'msg'=> 'Não encontrada  Vaga'
            ],404);
        };





        $deleted = DB::table('vagas')->where('Id', $vaga->Id)->delete();


        return response()->json(['msg'=>'Vaga Deletada com Sucesso!'],200);

    }

    public function vagasByType($type_id){
        $user = $this->request->user();


        $vagas = DB::select("SELECT Id,tipo_vaga_id,Titulo,Descricao,Salario,Curriculo_Linkedin,created_at,updated_at FROM vagas WHERE   tipo_vaga_id = '$type_id'");


        return response()->json($vagas,200);
    }






}
