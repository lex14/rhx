<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Analista;
use App\Models\User;
use App\Models\TipoVaga;
use Illuminate\Support\Facades\Validator;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;





use JWTAuth;


class TipoVagaController extends Controller
{
    protected $request;
    private $repository;

    public function __construct(Request $request,TipoVaga $tipo_vaga,Analista $analista)
    {
        $this->request = $request;
        $this->repository = $tipo_vaga;
        $this->analistaRepo = $analista;



    }


    public function create (){

        $user = $this->request->user();
        $validator =   $this->validateBody();

        if(!$validator->fails()){
            try{

                $data = $this->request->all();


                $tipo_vaga = $this->repository->create($data);

                $analista = $this->analistaRepo->where('Id',$user->id)->first();


                DB::update('UPDATE tipo_vaga SET analista_id = ? WHERE Id = ?', [$analista->Id,$tipo_vaga->id]);

                return response()->json($tipo_vaga,200);
            }catch(\Exception $e){
                if(str_contains($e->getMessage(),'unique')){
                    return response()->json(['msg'=>'Nome já utilizado!'],400);
                }
                return response()->json(['msg'=>'Erro ao Registrar Tipo de Vaga'],400);
              }
        }else{
            return response()->json($validator->errors(),400);
        }

    }



    public function validateBody(){
       $validate = Validator::make($this->request->all(),[
            'Nome'=>'required'
        ]);

        return $validate;

    }

    public function update($type_id){

        $user = $this->request->user();

        $tipo_vaga = $this->repository->where('Id',$type_id)->first();


        if(!$tipo_vaga){
            return response()->json([
                'msg'=> 'Não encontrado Tipo de Vaga'
            ],404);
        }

        $validator =   $this->validateBody();

        if(!$validator->fails()){
            try{

                $data = $this->request->all();


                $tipo_vaga->Nome = $data['Nome'];

                $tipo_vaga->save();


                $analista = $this->analistaRepo->where('Id',$user->id)->first();


                DB::update('UPDATE tipo_vaga SET analista_id = ? WHERE Id = ?', [$analista->Id,$tipo_vaga->id]);

                return response()->json(['msg'=>'Tipo de Vaga Atualizada com Sucesso!'],200);
            }catch(\Exception $e){
                if(str_contains($e->getMessage(),'unique')){
                    return response()->json(['msg'=>'Nome já utilizado!'],400);
                }
                return response()->json(['msg'=>'Erro ao Atualizar Tipo de Vaga'],400);
              }
        }else{
            return response()->json($validator->errors(),400);
        }


    }

    public function delete($type_id){

        $user = $this->request->user();

        $tipo_vaga = $this->repository->where('Id',$type_id)->first();

        if(!$tipo_vaga){
            return response()->json([
                'msg'=> 'Não encontrado Tipo de Vaga'
            ],404);
        };

        $exists = DB::select("SELECT Id FROM vagas WHERE   tipo_vaga_id = '$tipo_vaga->Id'");

        if(count($exists) > 0){
            return response()->json(['msg'=>'Tipo de Vaga não pode ser deletado pois está ligado á alguma vaga urgente '],400);
        }



        $deleted = DB::table('tipo_vaga')->where('Id', $tipo_vaga->Id)->delete();


        return response()->json(['msg'=>'Tipo de Vaga Deletada com Sucesso!'],200);

    }






}
