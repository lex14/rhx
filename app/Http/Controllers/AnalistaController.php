<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Analista;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;





use JWTAuth;


class AnalistaController extends Controller
{
    protected $request;
    private $repository;

    public function __construct(Request $request,Analista $analista,User $user)
    {
        $this->request = $request;
        $this->repository = $analista;
        $this->userRepo = $user;



    }


    public function register(){

        $validator =   $this->validateBody();

        if(!$validator->fails()){
            try{

                $data = $this->request->all();


                $data['Senha'] = Hash::make($data['Senha']);


                $analista = $this->repository->create($data);

                $user = $this->userRepo->create([
                    'password' => $data['Senha'],
                    'name' => $data['Nome'],
                    'email' => $data['Email'],
                ]);

                return response()->json($analista,200);
            }catch(\Exception $e){
                if(str_contains($e->getMessage(),'unique')){
                    return response()->json(['msg'=>'Email já utilizado!'],400);
                }
                return response()->json(['msg'=>'Erro ao Registrar Analista'],400);
              }
        }else{
            return response()->json($validator->errors(),400);
        }

    }

    public function login(){

          $validator =   $this->validateLogin();

            if(!$validator->fails()){
                $credentials = $this->request->only('Email', 'Senha');

                if($token = JWTAuth::attempt(
                    [
                        'password'=>$credentials['Senha'],
                        'email'=>$credentials['Email'],
                    ],
                    ['exp' => Carbon::now()->addMinutes(5)->timestamp]

                )){
                    $user = $this->userRepo->where('email',$credentials['Email'])->first();

                    return response()->json(['token' => $token,],200);
                }else{
                    return response()->json(['msg'=>'Erro ao logar'],400);
                }
            }else{
                return response()->json($validator->errors(),400);
            }
    }


    public function me(){

        $user = $this->request->user();

        $analista = $this->repository->where('Id',$user->id)->first();

        return response()->json($analista,200);

    }

    public function tiposVagas(){

        $user = $this->request->user();

        $analista = $this->repository->where('Id',$user->id)->first();


        $tiposVagas = DB::select("SELECT Id,Nome,created_at,updated_at FROM tipo_vagaWHERE analista_id = '$analista->Id'");


        return response()->json($tiposVagas,200);

    }


    public function vagas(){

        $user = $this->request->user();

        $analista = $this->repository->where('Id',$user->id)->first();


        $vagas = DB::select("SELECT Id,tipo_vaga_id,Titulo,Descricao,Salario,Curriculo_Linkedin,created_at,updated_at FROM vagas WHERE   analista_id = '$analista->Id'");


        return response()->json($vagas,200);

    }


    public function validateBody(){
       $validate = Validator::make($this->request->all(),[
            'Nome'=>'required',
            'Sobrenome'=>'required',
            'Email'=>'required|email',
            'Senha'=>'required|alphaNum'
        ]);

        return $validate;

    }

    public function validateLogin(){
        $validate = Validator::make($this->request->all(),[
             'Email'=>'required|email',
             'Senha'=>'required|alphaNum'
         ]);

         return $validate;

     }



}
