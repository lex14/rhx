<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Vaga extends Model
{


    protected $table = 'vagas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'Titulo','analista_id','tipo_vaga_id','Descricao','Salario','Curriculo_Linkedin'
    ];




}
