<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Analista extends Model
{


    protected $table = 'analistas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'Nome',
        'Sobrenome',
        'Email',
        'Senha'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'Senha',
    ];




}
