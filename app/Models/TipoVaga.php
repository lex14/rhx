<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TipoVaga extends Model
{


    protected $table = 'tipo_vaga';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'Nome','analista_id'
    ];




}
