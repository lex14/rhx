<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register ',[\App\Http\Controllers\AnalistaController::class,'register']);
Route::post('/login ',[\App\Http\Controllers\AnalistaController::class,'login'])->name('login');



Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('/me',[\App\Http\Controllers\AnalistaController::class,'me']);
    Route::get('/type/me',[\App\Http\Controllers\AnalistaController::class,'tiposVagas']);
    Route::post('/type/update/{type_id}',[\App\Http\Controllers\TipoVagaController::class,'update']);
    Route::post('/type/delete/{type_id}',[\App\Http\Controllers\TipoVagaController::class,'delete']);
    Route::post('/type/create',[\App\Http\Controllers\TipoVagaController::class,'create']);

    Route::get('/job/me',[\App\Http\Controllers\AnalistaController::class,'vagas']);
    Route::get('/job/type/{type_id}',[\App\Http\Controllers\VagaController::class,'vagasByType']);

    Route::post('/job/type/update/{news_id}',[\App\Http\Controllers\VagaController::class,'update']);
    Route::post('/job/type/delete/{news_id}',[\App\Http\Controllers\VagaController::class,'delete']);
    Route::post('/job/type/create',[\App\Http\Controllers\VagaController::class,'create']);
});
