<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vagas', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Titulo');
            $table->string('Descricao');
            $table->decimal('Salario',10,2);
            $table->string('Curriculo_Linkedin');
            $table->unsignedInteger('analista_id')->nullable();
            $table->unsignedInteger('tipo_vaga_id')->nullable();
            $table->timestamps();

            $table->foreign('analista_id')->references('Id')->on('analistas')

            ->onDelete('no action');

            $table->foreign('tipo_vaga_id')->references('Id')->on('tipo_vaga')

            ->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vagas');
    }
};
