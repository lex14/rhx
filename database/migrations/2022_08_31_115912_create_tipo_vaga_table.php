<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_vaga', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Nome')->unique();
            $table->unsignedInteger('analista_id')->nullable();
            $table->timestamps();

            $table->foreign('analista_id')->references('Id')->on('analistas')

            ->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_vaga');
    }
};
